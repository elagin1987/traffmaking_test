<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06.02.18
 * Time: 21:49
 */

use app\AutoRiaParser;
use app\RssCnn;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


include 'vendor/autoload.php';

$parserAutoria = new AutoRiaParser('https://auto.ria.com/legkovie/audi/a6/');
$parserAutoria->parsePage ();

$rss = new RssCnn();
$rss->parseRss ();