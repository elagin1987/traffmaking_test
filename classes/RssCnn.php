<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.02.18
 * Time: 14:59
 */

namespace app;


class RssCnn
{
    protected $url = 'http://rss.cnn.com/rss/edition_sport.rss';

    protected $items = [];

    public function __construct()
    {
        if(empty($this->url)){
            new \LogicException('Empty url to parse');
        }
    }

    public function parseRss()
    {
        $xml = simplexml_load_file('http://rss.cnn.com/rss/edition_sport.rss');

        $parsed_results_array = array();

        foreach($xml as $i=>$entry) {

            foreach($entry->item as $item) {

                $media = $item->children('media', true)->group->content->attributes ();

                $data = $this->xml2array ($media);

                $items = [];

                $items['img'] = $data['@attributes']['url'];

                $items['title'] = (string) $item->title;

                $items['description'] = (string) $item->description;

                $items['link'] = (string) $item->link;

                $parsed_results_array[] = $items;

                RssItem::create (
                    $items['img'],
                    $items['title'],
                    $items['description'],
                    $items['link']
                )->export ();
            }
        }
    }

    protected function xml2array ( $xmlObject, $out = array () )
    {
        foreach ( (array) $xmlObject as $index => $node )
            $out[$index] = ( is_object ( $node ) ) ? $this->xml2array ( $node ) : $node;

        return $out;
    }
}