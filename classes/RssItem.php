<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.02.18
 * Time: 14:59
 */

namespace app;


class  RssItem
{
    protected $img;
    protected $description;
    protected $link;
    protected $title;

    public function __construct($img, $title, $description, $link)
    {
        $this->title = $title;
        $this->description = $description;
        $this->img = $img;
        $this->link = $link;
    }

    public function export()
    {
        echo "Title:{$this->title}, img:{$this->img}, link:{$this->link}, description:{$this->description}".PHP_EOL;
    }

    static function create($img, $title, $description, $link){
        return new RssItem($img,
            $title,
            $description,
            $link
        );
    }
}