<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.02.18
 * Time: 14:58
 */

namespace app;


class AutoRiaParserPreview
{
    protected $title;
    protected $img;
    protected $url;

    public function __construct($img, $title, $url)
    {
        $this->img = $img;
        $this->title = $title;
        $this->url = $url;

        if(empty($img)){ new \LogicException("Empty param img");}
        if(empty($title)){ new \LogicException("Empty param title");}
        if(empty($url)){ new \LogicException("Empty param url");}
    }

    public function export()
    {
        echo "Title: {$this->title}, img: {$this->img}, url:{$this->url}".PHP_EOL;
    }
}