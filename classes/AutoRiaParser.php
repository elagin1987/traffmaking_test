<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.02.18
 * Time: 14:57
 */

namespace app;

use Exception;
use ErrorException;
use Sunra\PhpSimple\HtmlDomParser;

class AutoRiaParser
{
    protected $url = "https://auto.ria.com/legkovie/audi/a6/";

    public function __construct($url)
    {
        $this->url = $url;
    }

    /*
     * parse html, get link, img,title
     */
    public function parsePage()
    {
        if(empty($this->url)){new ErrorException('Empty url');}

        try {

            $html = HtmlDomParser::str_get_html( $this->getPage () );

            foreach ($html->find('div.ticket-photo loaded>a') as $link)
            {
                $picture = $link->find ('img', 0);

                $preview = new AutoRiaParserPreview($picture->src, $link->title, $link->href);

                $preview->export ();
            }
        }
        catch(Exception $e)
        {
            print $e->getMessage();
        }


    }

    /*
     * get html page
     */
    protected function getPage()
    {
        if(!$html = file_get_contents($this->url))
        {
            throw new Exception("Load Failed url-{$this->url}");
        }

        return $html;
    }
}